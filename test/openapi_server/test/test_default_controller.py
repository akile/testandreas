# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_wsgi_get_greeting(self):
        """Test case for wsgi_get_greeting

        Generate greeting
        """
        response = self.client.open(
            '/greeting/{name}'.format(name=dave),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
