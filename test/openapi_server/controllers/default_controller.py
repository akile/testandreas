import connexion
import six

from openapi_server import util


def wsgi_get_greeting(name):  # noqa: E501
    """Generate greeting

    Generates a greeting message. # noqa: E501

    :param name: Name of the person to greet.
    :type name: str

    :rtype: str
    """
    return 'do some magic!'
